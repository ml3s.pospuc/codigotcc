# -*- coding: utf-8 -*-

import cv2
import os

arquivos = os.scandir('rostos')

for arquivo in arquivos:
    imagem = cv2.imread(arquivo.path)
    imagemPB = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
    imagemResize = cv2.resize(imagemPB, (200,200))
    
    #salvar imagem
    cv2.imwrite(os.getcwd() + '/preparacao/convertido/' + arquivo.name , imagemResize)

    #exibir imagem
    '''
    cv2.imshow('ImagemConvertida', imagemResize)    
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    '''