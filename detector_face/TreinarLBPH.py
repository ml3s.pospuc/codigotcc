import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
import pkg_resources

print('Inicializando o treinador de LBPH')

#============================= funções
#funcao que detecta um rosto
def detectar_rosto(caminho_imagem):
    img = cv2.imread(caminho_imagem)
    
    rostos_detectados = modelo_detectorface.detectMultiScale(img, 1.3, 5)
    x, y, w, h = rostos_detectados[0] #pega o primeiro detectado, é o mais relevante 
    
    #tratando a imagem
    img = img[y:y+h, x:x+w] 
    img = cv2.resize(img, (224, 224))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    return img



#Detector de face
modelo_detectorface = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')


#criando o modelo LBPH
modelo = cv2.face.LBPHFaceRecognizer_create()

rostos_caminho =[ 
    "rostos/carlsen.jpg",
    "rostos/karpov.jpg",
    "rostos/kasparov.jpeg",
    "rostos/fischer.jpg",
    "rostos/capablanca.jpg"
]

rostos= []
for caminho in rostos_caminho:
    img = detectar_rosto(caminho)
    rostos.append(img)

ids = np.array([i for i in range(0, len(rostos))])

#Treinar
print("iniciando o treinamento")
modelo_treinado = "lbph_treinado.yml"
modelo.train(rostos, ids)
modelo.save(modelo_treinado)

#Exibir o histograma

if True:
    histograms = modelo.getHistograms()

    for i in range(0, len(rostos_caminho)):
        histogram = histograms[i][0]
        
        axis_values = np.array([i for i in range(0, len(histogram))])

        fig = plt.figure(figsize=(10, 5))
        
        ax1 = fig.add_subplot(1,2,1)
        plt.imshow(cv2.imread(rostos_caminho[i])[:,:,::-1])
        plt.axis('off')
        
        ax1 = fig.add_subplot(1,2,2)
        plt.bar(axis_values, histogram)
        plt.show()
        print("-----------------------")


def obterRosto(caminhoArquivo):
    
    img = detectar_rosto(caminhoArquivo)
        
    idx, confianca = modelo.predict(img)
    
    fig = plt.figure()

    ax1 = fig.add_subplot(1,2,1)
    plt.imshow(cv2.imread(caminhoArquivo)[:,:,::-1])
    plt.axis('off')

    ax1 = fig.add_subplot(1,2,2)
    plt.imshow(cv2.imread(rostos_caminho[idx])[:,:,::-1])
    plt.axis('off')

    plt.show()
    
    print("Confiança:: ", round(confianca, 2))


def exibirHistograma(caminhoArquivo):
    img = detectar_rosto(caminhoArquivo)
    tmp_model = cv2.face.LBPHFaceRecognizer_create()
    tmp_model.train([img], np.array([0]))
    
    histogram = tmp_model.getHistograms()[0][0]
    axis_values = np.array([i for i in range(0, len(histogram))])
    
    plt.bar(axis_values, histogram)
    plt.show()

#obterRosto("rostos_teste/fischer2.jpg")
#exibirHistograma("rostos/fischer.jpg")
