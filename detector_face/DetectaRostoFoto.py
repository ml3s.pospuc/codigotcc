# -*- coding: utf-8 -*-

import cv2
import numpy as np
import face_recognition
import time
print ("Inicializando detector de rosto")

#modelo_treinado = cv2.CascadeClassifier('cascade.xml')
modelo_treinado = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
#modelo_treinado = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_righteye_2splits.xml')
#modelo_treinado = cv2.CascadeClassifier('/home/marcos/Pessoal/pos/disciplinas/15-TCC/codigo/Mouth.xml')
#modelo_treinado = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_smile.xml')



imagem = cv2.imread('rostos_teste/gato.jpeg')


inicio = time.process_time()
locations = face_recognition.face_locations(imagem);
fim = time.process_time()
total = fim - inicio

print ('Tempo processamento: ')
print (total)

print(locations)
imagem_pb = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)

coordenadas = modelo_treinado.detectMultiScale(imagem_pb)



print(coordenadas[0])

(x,y,w,h) = coordenadas[0]
for coord in coordenadas:
    (x,y,w,h) = coord
    cv2.rectangle(imagem, (x,y), (x+w , y+h), (50,155,0),4 )

#(x2,y2,w2,h2) = coordenadas2[0]
#cv2.rectangle(imagem, (x2,y2), (x2+w2 , y2+h2), (0,100,200),3 )

cv2.imshow('Pessoa', imagem)
cv2.waitKey()