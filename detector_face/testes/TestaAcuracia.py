# -*- coding: utf-8 -*-

import cv2
import os
import numpy as np

#verifica a acurácia do detector de face a partir de arquivos onde existe
#rostos e arquivos que não existe
print ("Inicializando teste Acurácia...")

path_existeRosto = '/home/marcos/Pessoal/pos/disciplinas/15-TCC/TreinamentoHaar/p/'
path_nao_existeRosto = '/home/marcos/Pessoal/pos/disciplinas/15-TCC/TreinamentoHaar/n/'

arquivosPositivos = os.scandir(path_existeRosto)
arquivosNegativos = os.scandir(path_nao_existeRosto)


modelo_treinado = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
#modelo_treinado = cv2.CascadeClassifier('cascade.xml')


def verificar(arquivos, tipo):
    contador = 0
    contadorEncontrado = 0
    contadorNaoEncontrado = 0
    #verificando positivos
    print("Verificando " + tipo + " .....")

    for arquivo in arquivos:
        imagem = cv2.imread(arquivo.path)
        imagemPB = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
        imagemResize = cv2.resize(imagemPB, (200,200))
        contador=contador+1

        coordenadas = modelo_treinado.detectMultiScale(imagemPB)

        try:
            coord = coordenadas[0]
            contadorEncontrado = contadorEncontrado + 1
        except:
            contadorNaoEncontrado = contadorNaoEncontrado + 1


    
    print("Total imagens: " + str(contador))
    print("Total encontrado: " + str(contadorEncontrado))
    print("porcentagem positiva: " + str(contadorEncontrado/contadorNaoEncontrado))
    print("porcentagem negativa: " + str(contadorNaoEncontrado/contador))





verificar(arquivosPositivos, 'Positivos')
verificar(arquivosNegativos, 'Negativos')


