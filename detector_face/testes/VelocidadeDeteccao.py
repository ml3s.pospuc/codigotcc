# -*- coding: utf-8 -*-

import cv2
import os
import numpy as np
import face_recognition
import time
import matplotlib.pyplot as plt
import gc

#compara a velocidade de detecação entre os algoritmos Haar cascade e face_detection (dlib/CNN)
print ("Inicializando Teste de velocididade")

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


#path_existeRosto = '/home/marcos/Pessoal/pos/disciplinas/15-TCC/baseimagens/testeRapido/'
path_existeRosto = '/home/marcos/Pessoal/pos/disciplinas/15-TCC/baseimagens/img_align_celeba/'
arquivosPositivos = os.scandir(path_existeRosto)
arquivosPositivosFD = os.scandir(path_existeRosto)



modeloHaar = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

#Iniciando Haar
inicioHaar = time.process_time()

xHaar = [0]
yHaar = [0]
contador = 0
for arquivo in arquivosPositivos:
    if contador > 1000:
        break
    contador = contador + 1
    yHaar.append(contador)
    tempoParcial = (time.process_time() - inicioHaar)
    print("tempo Parcial Haar: " + str(tempoParcial))

    xHaar.append(tempoParcial)
    imagem = cv2.imread(arquivo.path)
    coordenadas = modeloHaar.detectMultiScale(imagem)

fimHaar = time.process_time()
totalHaar = fimHaar - inicioHaar
print("modelo Haar demorou: " + str(totalHaar))

#limpando cache
gc.collect()

#Iniciando face_detect
inicio = time.process_time()

y = [0]
x =[0]
contador = 0 
for arquivo in arquivosPositivosFD:
    if contador > 1000:
        break
    
    contador = contador + 1 
    y.append(contador)
    tempoParcial = (time.process_time() - inicio)    
    print("tempo Parcial: " + str(tempoParcial))

    x.append(tempoParcial)
    imagem = cv2.imread(arquivo.path)
    faceLocations = face_recognition.face_locations(imagem)

fim = time.process_time()
total = fim - inicio
print("modelo face_detect demorou: " + str(total))
fig,ax = plt.subplots()
ax.plot(x,y, label = 'Face detect')
ax.plot(xHaar, yHaar, label ='Haar')
plt.title('Comparativo Performance Haar x face_detect')
plt.xlabel('Tempo')
plt.ylabel('Quantidade imagens Processadas')
plt.legend()
plt.show()



