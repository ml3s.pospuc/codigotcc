# -*- coding: utf-8 -*-

import cv2
import numpy as np
import face_recognition
print ("Inicializando detector de Rosto")

modelo_treinado = cv2.CascadeClassifier('stage1.xml')
#modelo_treinado_openCV = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

webcam = cv2.VideoCapture(0)


while True:
    sucesso_leitura, quadro = webcam.read()
    imagem_pb = cv2.cvtColor(quadro, cv2.COLOR_BGR2GRAY)

    coordenadas = modelo_treinado.detectMultiScale(imagem_pb)
    #coordenadas_openCV = modelo_treinado_openCV.detectMultiScale(imagem_pb)    

    

    try:
        (x,y,w,h) = coordenadas[0]
        cv2.rectangle(quadro, (x,y), (x+w , y+h), (0,255,0),3 )
    except:
        print ('erro ao desenhar quadro identificador modelo treinado')


    try:
        (x2,y2,w2,h2) = coordenadas_openCV[0]
        cv2.rectangle(quadro, (x2,y2), (x2+w2 , y2+h2), (0,100,200),1 )
    except:
        print ('erro ao desenhar quadro identificador openCV')
    

    cv2.imshow('Pessoa', quadro)
    cv2.waitKey(1)





#cv2.imshow('Pessoa', imagem)
#cv2.waitKey()