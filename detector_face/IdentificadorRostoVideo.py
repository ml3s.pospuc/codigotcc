# -*- coding: utf-8 -*-

import cv2
import numpy as np
import face_recognition

print ("Inicializando Identificador")


#imagem = cv2.imread('pessoa3.jpg')

#Carrega rostos conhecidos
rostoKarpov = face_recognition.load_image_file('rostos/karpov.jpg')
rostoKasparov = face_recognition.load_image_file('rostos/kasparov.jpeg')
rostoCarlsen = face_recognition.load_image_file('rostos/carlsen.jpg')


#gerar Identificacao
encodeKarpov = face_recognition.face_encodings(rostoKarpov)[0]
encodeKasparov = face_recognition.face_encodings(rostoKasparov)[0]
encodeCarlsen = face_recognition.face_encodings(rostoCarlsen)[0]



webcam = cv2.VideoCapture(0)


while True:

    
    sucesso_leitura, quadro = webcam.read()
    imagem_pb = cv2.cvtColor(quadro, cv2.COLOR_BGR2GRAY)



    try:
        faceLocation = face_recognition.face_locations(quadro)[0]
        print(faceLocation); 
        cv2.rectangle(quadro, (faceLocation[3],faceLocation[0]), (faceLocation[1], faceLocation[2]), (200,100,50),2 )
    except:
        print('.')

    rosto_provavel = 'NAO-IDENTIFICADO'

    try:
        encodeWeb = face_recognition.face_encodings(quadro)[0]
        resultado = face_recognition.compare_faces([encodeKarpov, encodeKasparov, encodeCarlsen], encodeWeb)
        print('Resultado: ')
        print(resultado)
        if resultado[0] == True:
            rosto_provavel = 'KARPOV'
        if resultado[1] == True:
            rosto_provavel = 'KASPAROV'         
        if resultado[2] == True:
            rosto_provavel = 'CARLSEN'   
       
        
        y1, x2, y2, x1 = faceLocation

        cv2.putText(quadro, rosto_provavel, (x1,y1-6), cv2.FONT_HERSHEY_TRIPLEX, 1, (255,0,0), 3)
        print(rosto_provavel)
    except:
        print('erro encode webcam')

    '''
    (x,y,w,h) = coordenadas[0]
    cv2.rectangle(quadro, (x,y), (x+w , y+h), (0,255,0),3 )

    (x2,y2,w2,h2) = coordenadas2[0]
    cv2.rectangle(quadro, (x2,y2), (x2+w2 , y2+h2), (0,100,200),3 )
    '''


    

    cv2.imshow('Pessoa', quadro)
    cv2.waitKey(1)





#cv2.imshow('Pessoa', imagem)
#cv2.waitKey()