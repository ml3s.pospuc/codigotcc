# -*- coding: utf-8 -*-

import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
import pkg_resources

print('Inicializando o Identificador de foto com LBPH')


#funcao que detecta um rosto
def detectar_rosto(caminho_imagem):
    img = cv2.imread(caminho_imagem)
    
    rostos_detectados = modelo_detectorface.detectMultiScale(img, 1.3, 5)
    x, y, w, h = rostos_detectados[0] #pega o primeiro detectado, é o mais relevante 
    
    #tratando a imagem
    img = img[y:y+h, x:x+w] 
    img = cv2.resize(img, (224, 224))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    return img



#Detector de face
modelo_detectorface = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')


#criando o modelo LBPH
modelo = cv2.face.LBPHFaceRecognizer_create()

rostos_caminho =[ 
    "rostos/carlsen.jpg",
    "rostos/karpov.jpg",
    "rostos/kasparov.jpeg",
    "rostos/fischer.jpg",
    "rostos/capablanca.jpg"
]

rostos= []
for caminho in rostos_caminho:
    img = detectar_rosto(caminho)
    rostos.append(img)

ids = np.array([i for i in range(0, len(rostos))])

#Obtém modelo treinado
print("Obtendo modelo treinado")
modelo_treinado = "lbph_treinado.yml"
modelo.read(modelo_treinado)


def detectarRosto(caminhoArquivo):
    
    img = detectar_rosto(caminhoArquivo)
        
    idx, confianca = modelo.predict(img)

    print (idx)
    print("Confiança:: ", round(confianca, 2))
    
    fig = plt.figure()

    ax1 = fig.add_subplot(1,2,1)
    plt.imshow(cv2.imread(caminhoArquivo)[:,:,::-1])
    plt.axis('off')

    ax1 = fig.add_subplot(1,2,2)
    plt.imshow(cv2.imread(rostos_caminho[idx])[:,:,::-1])
    plt.axis('off')

    plt.show()   
    



#detectarRosto("rostos_teste/carlsen2.jpg")
#detectarRosto("rostos_teste/kasparov5.png")
detectarRosto("rostos_teste/capablanca.jpg")



