# -*- coding: utf-8 -*-

import cv2
import numpy as np
import face_recognition

print ("Inicializando Identificador por Foto")


imagem = cv2.imread('rostos_teste/karpovfischer.jpeg')

#Carrega rostos conhecidos
rostoKarpov = face_recognition.load_image_file('rostos/karpov.jpg')
rostoKasparov = face_recognition.load_image_file('rostos/kasparov.jpeg')
rostoCarlsen = face_recognition.load_image_file('rostos/carlsen.jpg')


#gerar Identificacao
encodeKarpov = face_recognition.face_encodings(rostoKarpov)[0]
encodeKasparov = face_recognition.face_encodings(rostoKasparov)[0]
encodeCarlsen = face_recognition.face_encodings(rostoCarlsen)[0]


for faceLocation in face_recognition.face_locations(imagem):

    print(faceLocation); 
    cv2.rectangle(imagem, (faceLocation[3],faceLocation[0]), (faceLocation[1], faceLocation[2]), (200,100,50),2 )

    rosto_provavel = 'NAO-IDENTIFICADO'

    try:
        encodeWeb = face_recognition.face_encodings(imagem)[0]
        resultado = face_recognition.compare_faces([encodeKarpov, encodeKasparov, encodeCarlsen], encodeWeb)
        print('Resultado: ')
        print(resultado)
        if resultado[0] == True:
            rosto_provavel = 'KARPOV'
        if resultado[1] == True:
            rosto_provavel = 'KASPAROV'         
        if resultado[2] == True:
            rosto_provavel = 'CARLSEN'   
        
            
        y1, x2, y2, x1 = faceLocation

        cv2.putText(imagem, rosto_provavel, (x1,y1-6), cv2.FONT_HERSHEY_TRIPLEX, 1, (255,0,0), 3)
        print(rosto_provavel)
    except:
        print('Na leitura da foto')

    cv2.imshow('Pessoa', imagem)
    cv2.waitKey()