# -*- coding: utf-8 -*-

import cv2
import numpy as np
import face_recognition
import time

print ("Inicializando detector de rosto utilizando o Face Recognition")


imagem = cv2.imread('rostos_teste/gato.jpeg')

#obtendo localização das faces
inicio = time.process_time()
faceLocations = face_recognition.face_locations(imagem);
fim = time.process_time()
total = fim - inicio

print ('Tempo processamento: ')
print (total)

for faceLocation in faceLocations:
    print(faceLocation); 
    cv2.rectangle(imagem, (faceLocation[3],faceLocation[0]), (faceLocation[1], faceLocation[2]), (200,100,50),2 )



cv2.imshow('Pessoa', imagem)
cv2.waitKey()